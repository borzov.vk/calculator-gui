object FCalculator: TFCalculator
  Left = 0
  Top = 0
  Caption = 'Calculator'
  ClientHeight = 153
  ClientWidth = 312
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object sbCalculate: TSpeedButton
    Left = 223
    Top = 84
    Width = 81
    Height = 29
    Caption = 'Calculate'
    OnClick = sbCalculateClick
  end
  object sbProjectionEvaluator: TSpeedButton
    Left = 132
    Top = 24
    Width = 101
    Height = 41
    Caption = 'Evaluate projection'
    OnClick = sbProjectionEvaluatorClick
  end
  object eLeft: TEdit
    Left = 34
    Top = 88
    Width = 73
    Height = 21
    TabOrder = 0
  end
  object eRight: TEdit
    Left = 122
    Top = 88
    Width = 73
    Height = 21
    TabOrder = 1
  end
  object eCoordinate: TEdit
    Left = 34
    Top = 32
    Width = 73
    Height = 21
    TabOrder = 2
  end
end
