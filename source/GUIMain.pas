unit GUIMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.StdCtrls, BackendPluginInterface;

type
  TFCalculator = class(TForm)
    eLeft: TEdit;
    eRight: TEdit;
    sbCalculate: TSpeedButton;
    eCoordinate: TEdit;
    sbProjectionEvaluator: TSpeedButton;
    procedure sbCalculateClick(Sender: TObject);
    procedure sbProjectionEvaluatorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function  CreateCppDescendant : TComputator; external 'ComputationsCPP.bpl';


var
  FCalculator: TFCalculator;

implementation

uses
    ort_drom, DefaultComputator;
{$R *.dfm}


procedure TFCalculator.sbCalculateClick(Sender: TObject);
var
  LeftDouble: Double;
  RightDouble: Double;
  BackendResult: Double;
  Computator: TComputator;
begin
  if not TryStrToFloat(eLeft.Text, LeftDouble)
  or not TryStrToFloat(eRight.Text, RightDouble) then
    Exit;

  Computator := nil;
  try
//    Computator := TDefaultComputator.Create;
    Computator := CreateCppDescendant;
    BackendResult := Computator.Compute(LeftDouble, RightDouble);
  finally
    FreeAndNil(Computator);
  end;
  ShowMessage(FloatToStr(BackendResult));

end;

procedure TFCalculator.sbProjectionEvaluatorClick(Sender: TObject);
var
  CoordinateValue: Double;
  ComputatorResult: Double;
begin
  if not TryStrToFloat(eCoordinate.Text, CoordinateValue) then
    Exit;

  CoordTest(0, 0, 50, CoordinateValue, 1, CoordinateValue, ComputatorResult);
  ShowMessage(FloatToStr(ComputatorResult));
end;

end.
